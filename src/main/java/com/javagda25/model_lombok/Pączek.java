package com.javagda25.model_lombok;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
// requiredArgsConstructor
// getter,
// setter,
// EqualsAndHashCode,
// toString
public class Pączek {

    private String imie;
    private double rozmiar;
    private boolean zLukrem;

    private boolean zPosypka;
    private boolean zNadzieniem;
    private double cena;
}
