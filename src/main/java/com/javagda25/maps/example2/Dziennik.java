package com.javagda25.maps.example2;

import java.util.Map;

public class Dziennik {
    //    private List<Student> studentList;

    //    public void dodajStudenta(Student s) {
    //        studentList.add(s);
    //    }

    private Map<String, Student> studentMap;

    public void dodajStudenta(Student s) {
        studentMap.put(s.getIndeks(), s);
    }

    public void wypiszImieStudenta(String indeks){
        Student s = studentMap.get(indeks);

        System.out.println(s.getImie());
    }
}
