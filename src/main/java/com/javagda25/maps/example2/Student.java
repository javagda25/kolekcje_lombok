package com.javagda25.maps.example2;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Student {
    private String indeks;
    private String imie;
    private String nazwisko;
}
