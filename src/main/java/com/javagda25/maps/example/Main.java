package com.javagda25.maps.example;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class Main {
    public static void main(String[] args) {
        Map<Osoba, Integer> mapa = new HashMap<>();
        mapa.put(new Osoba("123"), 5);
        mapa.put(new Osoba("123"), 6);
        mapa.put(new Osoba("234"), 20);

        Osoba o = new Osoba("123");

        if (mapa.containsKey(o)) {
            System.out.println(mapa.get(o));
        }

        if (mapa.containsValue(20)) {
            System.out.println("Zawiera 20tke");
        }

        // zbiór kluczy
        for (Osoba osoba : mapa.keySet()) {
            System.out.println(osoba);
        }

        // iteracja przez zbiór wartości
        for (Integer value : mapa.values()) {
            System.out.println("wartość: " + value);
        }

        // Map.Entry - klasa Entry (klasa wewnętrzna, zapisana wewnątrz klasy Map)
        // entry set
        for (Entry<Osoba, Integer> wpis : mapa.entrySet()) {
            // entry to obiekt który zawiera klucz i wartość
            // czyli jest to pełny wpis.

            System.out.println("Klucz: " + wpis.getKey());
            System.out.println("Wartość: " + wpis.getValue());
        }

//        Map<String, List<Map<String, List<Integer>>>> slownik;
    }
}
